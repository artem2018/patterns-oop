package strategy.fly;

public class FlyWithWings implements FlyBehavoir {
    @Override
    public void fly() {
        System.out.println("I am flying by wings");
    }
}
