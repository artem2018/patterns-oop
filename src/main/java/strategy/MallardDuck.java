package strategy;

import strategy.fly.FlyWithWings;
import strategy.quck.PerformQuack;

public class MallardDuck extends Duck {

    public MallardDuck() {
        quackBehaviour = new PerformQuack() ;
        flyBehavoiur = new FlyWithWings();
    }

    @Override
    public void display() {
        System.out.println("Mallard duck");
    }
}
