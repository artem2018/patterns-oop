package strategy;

import strategy.fly.FlyBehavoir;
import strategy.quck.QuackBehavior;

public abstract class Duck {

    FlyBehavoir flyBehavoiur;
    QuackBehavior quackBehaviour;

    public Duck() {
    }

    public abstract void display();

    public void performFly(){
        flyBehavoiur.fly();
    }

    public void setFlyBehavoiur(FlyBehavoir behavoiur) {
        flyBehavoiur = behavoiur;
    }

    public void performQuack(){
        quackBehaviour.quack();
    }

    public void swim() {
        System.out.println("Duck swim");
    }
}
