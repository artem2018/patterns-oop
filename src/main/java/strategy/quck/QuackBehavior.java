package strategy.quck;

public interface QuackBehavior {

    void quack();

}
