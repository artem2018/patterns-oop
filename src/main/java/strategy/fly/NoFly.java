package strategy.fly;

public class NoFly implements FlyBehavoir {
    @Override
    public void fly() {
        System.out.println("I cannot fly...");
    }
}
