package strategy;

import strategy.fly.FlyWithWings;

public class StrategyTest {

    public static void main(String[] args) {
        Duck mallardDuck = new MallardDuck();
        mallardDuck.performFly();
        mallardDuck.performQuack();
        mallardDuck.display();

        System.out.println("-------------");

        Duck seackDuck = new SeackDuck();
        seackDuck.performFly();
        seackDuck.performQuack();
        seackDuck.display();

        seackDuck.setFlyBehavoiur(new FlyWithWings());
        seackDuck.performFly();
    }

}
