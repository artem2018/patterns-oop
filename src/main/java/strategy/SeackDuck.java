package strategy;

import strategy.fly.NoFly;
import strategy.quck.SilenceQuack;

public class SeackDuck extends  Duck {

    public SeackDuck() {
        flyBehavoiur = new NoFly();
        quackBehaviour = new SilenceQuack();
    }

    @Override
    public void display() {
        System.out.println("I am Seeck duck.... ");
    }
}
