package strategy.quck;

public class SilenceQuack implements QuackBehavior {
    @Override
    public void quack() {
        System.out.println("No quack...");
    }
}
